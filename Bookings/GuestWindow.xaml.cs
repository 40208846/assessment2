﻿/* GuestWindow.xaml.cs
 * 
 * Allows the GuestWindow to function
 * 
 * Written by: 40208846
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ReservationSystem
{
    public partial class GuestWindow : Window
    {
        // The booking to which the guests belong
        Booking currBooking;
        Guest currGuest;

        public GuestWindow()
        {
            InitializeComponent();
        }

        // Constructor
        public GuestWindow(ref Bookable booking)
        {
            InitializeComponent();
            // If the booking that the guests belong to is a basic booking then no more needs to be done
            if (booking is Booking)
            {
                currBooking = (Booking)booking;
            }
            // If the booking has been decorated with extras then the actual booking is needed
            else
            {
                Extras temp = (Extras)booking;
                currBooking = (Booking)temp.GetBooking();
            }
            // Show the customers in the booking
            GuestDataGrid.ItemsSource = currBooking.GuestList;
        }

        // Adds a guest with the information entered into the GUI
        private void AddGuestButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                currBooking.AddGuest(new Guest(GuestNameTextBox.Text, Int32.Parse(GuestAgeTextBox.Text), GuestPassportNumberTextBox.Text));
            }
            catch (ArgumentException argumentException)
            {
                MessageBox.Show(argumentException.Message);
            }
            catch (FormatException)
            {
                MessageBox.Show("Guest age must be a number");
            }
            
            // Updates the list of customers
            GuestDataGrid.ItemsSource = null;
            GuestDataGrid.ItemsSource = currBooking.GuestList;
        }

        private void RemoveGuestButton_Click(object sender, RoutedEventArgs e)
        {
            currGuest = (Guest)GuestDataGrid.SelectedItem;
            if (currGuest != null)
            {
                // Ask the user for confirmation
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure?", "Delete Confirmation", MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    // Remove the current booking from the user's list of bookings
                    currBooking.GuestList.Remove(currGuest);
                    GuestDataGrid.ItemsSource = null;
                    GuestDataGrid.ItemsSource = currBooking.GuestList;
                }
            }
            else
            {
                MessageBox.Show("You must select a guest from the list");
            } 
        }
    }
}
