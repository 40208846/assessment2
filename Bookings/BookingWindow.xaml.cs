﻿/* BookingWindow.xaml.cs
 * 
 * Allows the BookingWindow to function
 * 
 * Written by: 40208846
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ReservationSystem
{
    public partial class BookingWindow : Window
    {
        // Initialises variables needed for window to function
        public BuilderSingleton BookingManager;
        public Customer CurrCust;
        public Bookable CurrBooking;

        public BookingWindow()
        {
            InitializeComponent();
        }

        // Constructor
        public BookingWindow(ref BuilderSingleton builderSingleton, ref Customer customer)
        {
            InitializeComponent();
            BookingManager = builderSingleton;
            CurrCust = customer;
            BookingsDataGrid.ItemsSource = CurrCust.Bookings;
            //CurrBooking = (Bookable)BookingsDataGrid.SelectedItem;
        }

        // Adds booking described in GUI to the current customer's list of bookings
        private void AddBookingButton_Click(object sender, RoutedEventArgs e)
        {
            // Creates a booking with unique Booking Reference and the inputted values
            Booking temp = BookingManager.NewBooking(CurrCust, ArrivalDatePicker.SelectedDate ?? DateTime.Now, DepartureDatePicker.SelectedDate ?? DateTime.Now);
            CurrBooking = temp;

            // Adds the extras to booking
            setExtras(ref CurrBooking);

            // Adds the new booking to the Customer's list of bookings
            CurrCust.Bookings.Add(CurrBooking);

            // Restores all the inputs to their defaults then update the list of bookings
            clearAll();
            BookingsDataGrid.ItemsSource = null;
            BookingsDataGrid.ItemsSource = CurrCust.Bookings;
        }

        // Modifies current booking to match inputs to GUI
        private void EditCurrentBookingButton_Click(object sender, RoutedEventArgs e)
        {
            // If a booking is selected
            if (BookingsDataGrid.SelectedItem != null)
            {
                CurrBooking = (Bookable)BookingsDataGrid.SelectedItem;
                // Removes the booking from the customer's list of bookings
                CurrCust.Bookings.Remove(CurrBooking);
                CurrBooking.ArrivalDate = ArrivalDatePicker.SelectedDate ?? DateTime.Now;
                CurrBooking.DepartureDate = DepartureDatePicker.SelectedDate ?? DateTime.Now;
                setExtras(ref CurrBooking);

                // Adds the updated booking to the list of bookings
                CurrCust.Bookings.Add(CurrBooking);

                // Restores all the inputs to their defaults then update the list of bookings
                clearAll();
                BookingsDataGrid.ItemsSource = null;
                BookingsDataGrid.ItemsSource = CurrCust.Bookings;
            }
            else
            {
                MessageBox.Show("You must select a booking from the list");
            }

        }

        // Deletes current booking
        private void DeleteCurrentBookingButton_Click(object sender, RoutedEventArgs e)
        {
            // If a booking is selected
            if (BookingsDataGrid.SelectedItem != null)
            {
                CurrBooking = (Bookable)BookingsDataGrid.SelectedItem;
                // Ask the user for confirmation
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure?", "Delete Confirmation", MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    // Remove the current booking from the user's list of bookings
                    CurrCust.Bookings.Remove(CurrBooking);
                    BookingsDataGrid.ItemsSource = null;
                    BookingsDataGrid.ItemsSource = CurrCust.Bookings;
                }
            }
            else
            {
                MessageBox.Show("You must select a booking from the list");
            }


        }

        // Opens a window allowing guests to be displayed, added, and removed
        private void ShowGuestButton_Click(object sender, RoutedEventArgs e)
        {
            // If a booking is selected
            if (BookingsDataGrid.SelectedItem != null)
            {
                CurrBooking = (Bookable)BookingsDataGrid.SelectedItem;
                GuestWindow guestWindow = new GuestWindow(ref CurrBooking);
                guestWindow.ShowDialog();
            }
            else
            {
                MessageBox.Show("You must select a booking from the list");
            }

        }

        // Restores all GUI inputs to their default blank state
        private void clearAll()
        {
            ArrivalDatePicker.SelectedDate = null;
            DepartureDatePicker.SelectedDate = null;
            BreakfastsCheckBox.IsChecked = false;
            BreakfastsDietaryRequirementsTextBox.Text = "";
            EveningMealsCheckBox.IsChecked = false;
            EveningMealsDietaryRequirementsTextBox.Text = "";
            CarHireCheckBox.IsChecked = false;
            CarHireStartDatePicker.SelectedDate = null;
            CarHireEndDatePicker.SelectedDate = null;
            CarHireDriverNameTextBox.Text = "";
        }

        // Method used to add extras to booking following the decorator design pattern
        private void setExtras(ref Bookable bookingToBeAdded)
        {
            if (bookingToBeAdded.GetType() != typeof(Booking))
            {
                Extras bookingToBeAddedHasExtras = (Extras)bookingToBeAdded;
                bookingToBeAdded = bookingToBeAddedHasExtras.GetBooking();
            }
            if (BreakfastsCheckBox.IsChecked == true)
            {
                BreakfastExtra breakfastTemp = new BreakfastExtra(bookingToBeAdded, BreakfastsDietaryRequirementsTextBox.Text);
                bookingToBeAdded = breakfastTemp;
            }

            if (EveningMealsCheckBox.IsChecked == true)
            {
                EveningMealExtra eveningTemp = new EveningMealExtra(bookingToBeAdded, EveningMealsDietaryRequirementsTextBox.Text);
                bookingToBeAdded = eveningTemp;
            }

            if (CarHireCheckBox.IsChecked == true)
            {
                CarHireExtra carHireTemp = new CarHireExtra(bookingToBeAdded, CarHireStartDatePicker.SelectedDate ?? DateTime.Now, CarHireEndDatePicker.SelectedDate ?? DateTime.Now, CarHireDriverNameTextBox.Text);
                bookingToBeAdded = carHireTemp;
            }
        }

        // Method used to display the details of a booking with extras using the GUI
        private void getExtras(Bookable currBookable)
        {
            // Depending on the type of extra, set the GUI controls to show the booking information
            if (currBookable is BreakfastExtra)
            {
                BreakfastExtra currExtra = (BreakfastExtra)currBookable;
                BreakfastsCheckBox.IsChecked = true;
                BreakfastsDietaryRequirementsTextBox.Text = currExtra.DietaryRequirements;
                // Because an extra can decorate another extra recursion must be used
                getExtras(currExtra.Booking);

            }
            else if (currBookable is EveningMealExtra)
            {
                EveningMealExtra currExtra = (EveningMealExtra)currBookable;
                EveningMealsCheckBox.IsChecked = true;
                EveningMealsDietaryRequirementsTextBox.Text = currExtra.DietaryRequirements;
                // Because an extra can decorate another extra recursion must be used
                getExtras(currExtra.Booking);
            }
            else if (currBookable is CarHireExtra)
            {
                CarHireExtra currExtra = (CarHireExtra)currBookable;
                CarHireCheckBox.IsChecked = true;
                CarHireStartDatePicker.SelectedDate = currExtra.CarHireStartDate;
                CarHireEndDatePicker.SelectedDate = currExtra.CarHireEndDate;
                CarHireDriverNameTextBox.Text = currExtra.DriverName;
                // Because an extra can decorate another extra recursion must be used
                getExtras(currExtra.Booking);
            }
        }

        // Updates the current booking to the one selected in the DataGrid (list of bookings)
        private void BookingsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            clearAll();

            // If a booking is selected
            if (BookingsDataGrid.SelectedItem != null)
            {
                if (BookingsDataGrid.SelectedItem is Booking)
                {
                    CurrBooking = (Booking)BookingsDataGrid.SelectedItem;
                }
                else if (BookingsDataGrid.SelectedItem is BreakfastExtra)
                {
                    CurrBooking = (BreakfastExtra)BookingsDataGrid.SelectedItem;
                    getExtras(CurrBooking);
                }
                else if (BookingsDataGrid.SelectedItem is EveningMealExtra)
                {
                    CurrBooking = (EveningMealExtra)BookingsDataGrid.SelectedItem;
                    getExtras(CurrBooking);
                }
                else if (BookingsDataGrid.SelectedItem is CarHireExtra)
                {
                    CurrBooking = (CarHireExtra)BookingsDataGrid.SelectedItem;
                    getExtras(CurrBooking);
                }
                ArrivalDatePicker.SelectedDate = CurrBooking.ArrivalDate;
                DepartureDatePicker.SelectedDate = CurrBooking.DepartureDate;

            }

        }

        private void InvoiceButton_Click(object sender, RoutedEventArgs e)
        {
            if (CurrBooking.GetGuestCount() > 0)
            {
                InvoiceWindow invoiceWindow = new InvoiceWindow(ref CurrBooking);
                invoiceWindow.ShowDialog();
            }
            else
            {
                MessageBox.Show("There are no guests for this booking");
            }

        }
    }
}
