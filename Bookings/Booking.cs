﻿/* Booking.cs
 * 
 * Holds the information about the booking being made by the customer
 * 
 * Written by: 40208846
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem
{
    // Base Bookable class, allows Decoration to function
    [Serializable]
    public abstract class Bookable
    {
        // The date the guests will arrive at the holiday village
        private DateTime arrivalDate;

        public DateTime ArrivalDate
        {
            get { return arrivalDate; }
            set { arrivalDate = value; }
        }

        // The date the customers will leave the village, must not be before the arrival date
        private DateTime departureDate;

        public DateTime DepartureDate
        {
            get { return departureDate; }
            set
            {
                if (value < arrivalDate)
                {
                    throw new ArgumentException("Departure date must be after arrival date");
                }
                else
                {
                    departureDate = value;
                }
            }
        }

        protected Bookable(DateTime start, DateTime end)
        {
            ArrivalDate = start;
            DepartureDate = end;
        }

        // Methods inherited classes must implement
        public abstract void AddGuest(Guest newGuest);
        public abstract Dictionary<string, double> GetCost();
        public abstract int GetGuestCount();
    }

    // Base booking class without extras
    [Serializable]
    public class Booking : Bookable
    {
        // The booking reference, which should only be set when the booking is created
        private int bookingRef;

        public int BookingRef
        {
            get { return bookingRef; }
            set { }
        }

        // The customer to whom the booking belongs
        private Customer customer;

        public Customer Customer
        {
            get { return customer; }
            set { customer = value; }
        }

        // List of guests who will stay at holiday village
        private List<Guest> guestList;

        // GuestList has no setter to ensure that no more than 4 guests are in the booking
        public List<Guest> GuestList
        {
            get { return guestList; }
            set { }
        }

        // Booking constructor 
        public Booking(int reference, Customer newCust, DateTime start, DateTime end)
            : base(start, end)
        {
            bookingRef = reference;
            customer = newCust;
            guestList = new List<Guest>();
        }

        // Allows guests to be added to the booking, will not allow there to be more than 4 guests
        public override void AddGuest(Guest newGuest)
        {
            if (guestList.Count <= 3)
            {
                guestList.Add(newGuest);
            }
            else
            {
                throw new ArgumentException("A booking can only have 4 guests");
            }
        }

        // Calculates total cost of booking TODO: change this to invoice
        public override Dictionary<string, double> GetCost()
        {
            Dictionary<string, double> invoice = new Dictionary<string, double>();
            double total = 0;
            foreach (Guest guest in GuestList)
            {
                if (guest.Age <= 18)
                {
                    total += 30 * (DepartureDate - ArrivalDate).Days;
                }
                else
                {
                    total += 50 * (DepartureDate - ArrivalDate).Days;
                }
            }
            invoice.Add("Booking", total);
            return invoice;
        }

        // Returns the number of guests in the booking
        public override int GetGuestCount()
        {
            return GuestList.Count();
        }
    }
}
