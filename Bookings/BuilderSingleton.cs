﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem
{
    public class BuilderSingleton
    {
        private static BuilderSingleton instance;
        private int lastBookingId = 0;
        private int lastCustomerId = 0;

        // As BuilderSingleton follows the Singleton pattern the constructor is not public
        protected BuilderSingleton()
        { }

        // This is used to gain access to the single instance of this class
        public static BuilderSingleton Instance()
        {
            if (instance == null)
            {
                instance = new BuilderSingleton();
            }
            return instance;
        }

        // Method for creating a customer. Ensures that each customer has a unique customerID
        public Customer NewCustomer(string name, string address)
        {
            Customer temp;
            lastCustomerId++;
            try
            {
                temp = new Customer(lastCustomerId, name, address);
                return temp;
            }
            catch(ArgumentException)
            {
                lastCustomerId--;
            }
            return null;
            
            
        }

        // Method for creating a booking. Ensures that each booking has a unique bookingID
        public Booking NewBooking(Customer currcust, DateTime start, DateTime end)
        {
            lastBookingId++;
            Booking temp = new Booking(lastBookingId, currcust, start, end);
            return temp;
        }
    }
}
