﻿/* MainWindow.xaml.cs
 * 
 * Allows the MainWindow to function
 * 
 * Written by: 40208846
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace ReservationSystem
{
   
    public partial class MainWindow : Window
    {
        // Initialise the variables needed for the program to function
        public Facade ReservationSystemFacade = Facade.Instance();
        public BuilderSingleton BookingManager = BuilderSingleton.Instance();
        public Customer CurrCust;
        public Booking CurrBooking;

        public MainWindow()
        {
            InitializeComponent();
            ReservationSystemFacade.Deserialize();
        }

        // Button used to add customer
        private void AddCustButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Creates a new customer with unique ID
                CurrCust = BookingManager.NewCustomer(NewCustomerNameTextBox.Text, NewCustomerAddressTextBox.Text);
                ReservationSystemFacade.AddCustomer(CurrCust);

                // Updates GUI components to show added guest
                CurrentCustomerUpdate();
            }
            catch (ArgumentException addCustomerException)
            {
                if (addCustomerException.Message == "Name cannot be blank") 
                {
                    NewCustomerNameTextBox.BorderBrush = Brushes.Red;
                }
                else if (addCustomerException.Message == "Name cannot be blank")
                {
                    NewCustomerAddressTextBox.BorderBrush = Brushes.Red;
                }
                else
                {
                    NewCustomerNameTextBox.BorderBrush = Brushes.Red;
                    NewCustomerAddressTextBox.BorderBrush = Brushes.Red;
                }
            }
        }

        // Used to look up customer by CustomerID
        private void FindCustButton_Click(object sender, RoutedEventArgs e)
        {
            // If the inputted customer ID exists then show the information, otherwise highlight the box red
            try
            {
                CustomerReferenceLookUpTextBox.ClearValue(TextBox.BorderBrushProperty);
                CurrCust = ReservationSystemFacade.GetCustomer(System.Int32.Parse(CustomerReferenceLookUpTextBox.Text));
                NewCustomerNameTextBox.Text = CurrCust.Name;
                NewCustomerAddressTextBox.Text = CurrCust.Address;
                CurrentCustomerUpdate();
            }
            catch (FormatException)
            {
                CustomerReferenceLookUpTextBox.BorderBrush = Brushes.Red;
                MessageBox.Show("Customer reference must be an integer", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                CurrentCustomerUpdate();
            }
            catch (ArgumentException)
            {
                CustomerReferenceLookUpTextBox.BorderBrush = Brushes.Red;
                MessageBox.Show("Customer reference does not exist", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                CurrentCustomerUpdate();
            }
        }

        // Opens a window showing all the bookings the current customer has
        private void ShowBookingsButton_Click(object sender, RoutedEventArgs e)
        {
            if (CurrCust != null)
            {
                BookingWindow bookingWindow = new BookingWindow(ref BookingManager, ref CurrCust);
                bookingWindow.ShowDialog();
            }
            else
            {
                MessageBox.Show("You must have an active customer");
            }
        }

        // Deletes the current customer, providing they have no bookings
        private void DeleteCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            if (CurrCust != null)
            {
                // Customer can only be deleted if they have no bookings
                if (CurrCust.Bookings.Count() == 0)
                {
                    MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure?", "Delete Confirmation", MessageBoxButton.YesNo);
                    if (messageBoxResult == MessageBoxResult.Yes)
                    {
                        ReservationSystemFacade.RemoveCustomer(CurrCust.CustomerReference);
                        CurrCust = null;
                        CurrentCustomerUpdate();
                    }
                }
                else
                {
                    MessageBox.Show("Customer cannot be deleted while still having bookings");
                }
            }
            else
            {
                MessageBox.Show("You must have an active customer");
            }
        }

        // Updates all items showing the customer's information
        private void CurrentCustomerUpdate()
        {
            // Resets all values, just in case
            NewCustomerNameTextBox.Text = "";
            NewCustomerNameTextBox.ClearValue(TextBox.BorderBrushProperty);
            NewCustomerAddressTextBox.Text = "";
            NewCustomerAddressTextBox.ClearValue(TextBox.BorderBrushProperty);
            CustomerReferenceLookUpTextBox.Text = "";
            CustomerReferenceLookUpTextBox.ClearValue(TextBox.BorderBrushProperty);
            // If there is currently a customer then use their information
            if (CurrCust != null)
            {
                CurrentCustomerNameHolderLabel.Content = CurrCust.Name;
                CurrentCustomerNameHolderLabel.FontWeight = FontWeights.Bold;
                CurrentCustomerAddressHolderLabel.Content = CurrCust.Address;
                CurrentCustomerAddressHolderLabel.FontWeight = FontWeights.Bold;
                CurrentCustomerReferenceHolderLabel.Content = CurrCust.CustomerReference;
                CurrentCustomerReferenceHolderLabel.FontWeight = FontWeights.Bold;
            }
            // If there is not a customer selected then show that
            else
            {
                CurrentCustomerNameHolderLabel.Content = "[None Selected]";
                CurrentCustomerNameHolderLabel.FontWeight = FontWeights.Regular;
                CurrentCustomerAddressHolderLabel.Content = "[None Selected]";
                CurrentCustomerAddressHolderLabel.FontWeight = FontWeights.Regular;
                CurrentCustomerReferenceHolderLabel.Content = "[None Selected]";
                CurrentCustomerReferenceHolderLabel.FontWeight = FontWeights.Regular;
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            ReservationSystemFacade.Serialize();
        }
    }
}
