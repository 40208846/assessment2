﻿/* Facade.cs
 * 
 * Uses the facade and singleton design patterns.
 * Provides a single interface for several subsystems.
 * Makes the GUI code simpler.
 * 
 * Written by: 40208846
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace ReservationSystem
{
    public class Facade
    {
        public Dictionary<int, Customer> CustDictionary = new Dictionary<int, Customer>();
        private static Facade instance;
        private Stream stream;
        private BinaryFormatter formatter = new BinaryFormatter();

        // As BuilderSingleton follows the Singleton pattern the constructor is not public
        protected Facade()
        { }

        // This is used to gain access to the single instance of this class
        public static Facade Instance()
        {
            if (instance == null)
            {
                instance = new Facade();
            }
            return instance;
        }

        // Reads the stored information from the file
        public void Deserialize()
        {
            if (File.Exists("ReservationSystem.bin"))
            {
                stream = File.Open("ReservationSystem.bin", FileMode.Open);
                CustDictionary = (Dictionary<int, Customer>)formatter.Deserialize(stream);
                // As the customer reference may not be up to date in the BuilderSingleton it is updated
                BuilderSingleton builderSingleton = BuilderSingleton.Instance();
                foreach ( KeyValuePair<int, Customer> customer in CustDictionary)
                {
                    var temp = builderSingleton.NewCustomer("temp", "temp");
                }
                stream.Close();
            }
        }

        // Writes all the customers, bookings and guests to a binary file
        public void Serialize()
        {
            stream = File.Open("ReservationSystem.bin", FileMode.Create);
            formatter.Serialize(stream, CustDictionary);
            stream.Close();
        }

        // Adds a new customer to the list of customers
        public void AddCustomer(Customer newCustomer)
        {
            if (newCustomer != null)
            {
                CustDictionary.Add(newCustomer.CustomerReference, newCustomer);
            }
            else
            {
                throw new ArgumentException("Customer could not be created");
            }
            
        }

        // Returns a customer from their reference number, if they exist
        public Customer GetCustomer(int customerReference)
        {
            Customer temp;
            if (CustDictionary.TryGetValue(customerReference, out temp))
            {
                return temp;
            }
            else
            {
                throw new ArgumentException("Customer does not exist");
            }
        }

        // Removes a customer from the list of customers
        public void RemoveCustomer(int customerReference)
        {
            CustDictionary.Remove(customerReference);
        }
        
        // Gets the invoice from the current Booking and adds the total to the invoice
        public Dictionary<String, Double> Invoice(ref Bookable booking)
        {
            Dictionary<String, Double> invoiceList = booking.GetCost();
            double total = 0;
            foreach (KeyValuePair<string, double> item in invoiceList)
            {
                total += item.Value;
            }
            invoiceList.Add("Total", total);
            return invoiceList;
        }
    }
}
