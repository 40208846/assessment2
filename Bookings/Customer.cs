﻿/* Customer.cs
 * 
 * The customer booking the visits
 * 
 * Written by: 40208846
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem
{
    [Serializable]
    public class Customer
    {
        // Customer's name cannot be blank
        private string name;

        public string Name
        {
            get { return name; }
            set
            {
                if (value == "")
                {
                    throw new ArgumentException("Name cannot be blank");
                }
                else
                {
                    name = value;
                }

            }
        }

        // Customer's address cannot be blank
        private string address;

        public string Address
        {
            get { return address; }
            set
            {
                if (value == "")
                {
                    throw new ArgumentException("Address cannot be blank");
                }
                else
                {
                    address = value;
                }
            }
        }

        // CusomerReference has no setter because it can only be set when the object is created
        private int customerReference;

        public int CustomerReference
        {
            get { return customerReference; }
            set { }
        }

        // The list of bookings the customer holds
        private List<Bookable> bookings;

        public List<Bookable> Bookings
        {
            get { return bookings; }
            set { bookings = value; }
        }

        // Customer constructors
        public Customer(int reference)
        {
            customerReference = reference;
            bookings = new List<Bookable>();
        }

        public Customer(int reference, string newCustomerName, string newCustomerAddress)
        {
            customerReference = reference;
            Name = newCustomerName;
            Address = newCustomerAddress;
            bookings = new List<Bookable>();
        }
    }
}
