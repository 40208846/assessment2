﻿/* Extras.cs
 * 
 * This class follows the Decorator pattern to add extras to a base Bookable object
 * 
 * Includes base Extras, BreakfastExtra, EveningMealExtra and CarHireExtra
 * 
 * Written by: 40208846
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem
{
    // Base Extras class to allow the actual extras to correctly decorate the base Bookable
    [Serializable]
    public abstract class Extras : Bookable
    {
        // The bookbale object being decorated
        public Bookable Booking;

        // Gets the BookingReference of the original booking
        public int BookingRef
        {
            get
            {
                // While the current object is some form of extra
                while (!(Booking is Booking))
                {
                    // Get the booking from the extra being decorated
                    Extras extraWithBooking = (Extras)Booking;
                    return extraWithBooking.BookingRef;
                }
                // Gets the booking the extra has decorated
                Booking bookingToBeReferenced = (Booking)Booking;
                return bookingToBeReferenced.BookingRef;
            }
            // Because the BookingRef is being looked up it cannot be set
            set { }
        }

        // Base Extras constructor 
        public Extras(Bookable newBooking)
            : base(newBooking.ArrivalDate, newBooking.DepartureDate)
        {
            this.Booking = newBooking;
        }

        // Base Extras does not change the final cost so the GetCost method from the Booking is called
        public override Dictionary<string, double> GetCost()
        {
            return Booking.GetCost();
        }

        // Gets the booking being decorated
        public Bookable GetBooking()
        {
            // While the current object is some form of extra
            while (!(Booking is Booking))
            {
                // Get the booking from the extra being decorated
                Extras extraWithBooking = (Extras)Booking;
                return extraWithBooking.GetBooking();
            }
            // Gets the booking the extra has decorated
            Booking bookingToBeReferenced = (Booking)Booking;
            return bookingToBeReferenced;
        }

        // Allows guests to be added to the booking
        public override void AddGuest(Guest newGuest)
        {
            Booking.AddGuest(newGuest);
        }
    }

    [Serializable]
    public class EveningMealExtra : Extras
    {
        // The dietary requirements of the booking should be noted
        private string dietaryRequirements;

        public string DietaryRequirements
        {
            get { return dietaryRequirements; }
            set { dietaryRequirements = value; }
        }

        public EveningMealExtra(Bookable newBooking, string requirements)
            : base(newBooking)
        {
            DietaryRequirements = requirements;
        }

        // The cost will be an extra £15 per person per night
        public override Dictionary<string, double> GetCost()
        {

            Dictionary<string, double> invoice = base.GetCost();
            double total = 0;
            total += 15 * (Booking.DepartureDate - Booking.ArrivalDate).Days * Booking.GetGuestCount(); ;
            invoice.Add("Evening Meal", total);
            return invoice;
        }

        // The GetGuestCount method is required of all classes inheriting from Bookable, 
        // however no extra changes this therefore the GetGuestCount method from the booking is used
        public override int GetGuestCount()
        {
            return Booking.GetGuestCount();
        }
    }

    
    [Serializable]
    public class BreakfastExtra : Extras
    {
        // The dietary requirements of the booking should be noted
        private string dietaryRequirements;

        public string DietaryRequirements
        {
            get { return dietaryRequirements; }
            set { dietaryRequirements = value; }
        }

        public BreakfastExtra(Bookable newBooking, string requirements)
            : base(newBooking)
        {
            DietaryRequirements = requirements;
        }

        // The cost will be an extra £15 per person per night
        public override Dictionary<string, double> GetCost()
        {

            Dictionary<string, double> invoice = base.GetCost();
            double total = 0;
            total += 5 * (Booking.DepartureDate - Booking.ArrivalDate).Days * Booking.GetGuestCount(); ;
            invoice.Add("Breakfasts", total);
            return invoice;
        }

        // The GetGuestCount method is required of all classes inheriting from Bookable, 
        // however no extra changes this therefore the GetGuestCount method from the booking is used
        public override int GetGuestCount()
        {
            return Booking.GetGuestCount();
        }
    }

    [Serializable]
    public class CarHireExtra : Extras
    {
        // The following should be noted, hire start date, hire end date and name of driver
        private DateTime carHireStartDate;

        public DateTime CarHireStartDate
        {
            get { return carHireStartDate; }
            set { carHireStartDate = value; }
        }

        // The date for the end of the car hire cannot be before the hire has started
        private DateTime carHireEndDate;

        public DateTime CarHireEndDate
        {
            get { return carHireEndDate; }
            set
            {
                if (value < carHireStartDate)
                {
                    throw new ArgumentException("Departure date must be after arrival date");
                }
                else
                {
                    carHireEndDate = value;
                }
            }
        }

        // Driver name should not be blank
        private string driverName;

        public string DriverName
        {
            get { return driverName; }
            set
            {
                if (value == "")
                {
                    throw new ArgumentException("Driver name cannot be blank");
                }
                else
                {
                    driverName = value;
                }
            }
        }

        public CarHireExtra(Bookable newBooking, DateTime startHire, DateTime endHire, string name)
            : base(newBooking)
        {
            CarHireStartDate = startHire;
            CarHireEndDate = endHire;
            DriverName = name;
        }

        // Car hire costs £50 per day extra
        public override Dictionary<string, double> GetCost()
        {

            Dictionary<string, double> invoice = base.GetCost();
            double total = 0;
            total += 50 * (Booking.DepartureDate - Booking.ArrivalDate).Days * Booking.GetGuestCount(); ;
            invoice.Add("Car Hire", total);
            return invoice;
        }

        // The GetGuestCount method is required of all classes inheriting from Bookable, 
        // however no extra changes this therefore the GetGuestCount method from the booking is used
        public override int GetGuestCount()
        {
            return Booking.GetGuestCount();
        }
    }
}
