﻿/* MainWindow.xaml.cs
 * 
 * Allows the InvoiceWindow to function
 * 
 * Written by: 40208846
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ReservationSystem
{
    /// <summary>
    /// Interaction logic for InvoiceWindow.xaml
    /// </summary>
    public partial class InvoiceWindow : Window
    {
        Bookable booking;
        Facade facade = Facade.Instance();
        public InvoiceWindow()
        {
            InitializeComponent();
        }

        public InvoiceWindow(ref Bookable bookingToBeInvoiced)
        {
            InitializeComponent();
            booking = bookingToBeInvoiced;
            InvoiceDataGrid.ItemsSource = facade.Invoice(ref booking); ;
        }
    }
}
