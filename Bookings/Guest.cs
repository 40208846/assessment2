﻿/* Guest.cs
 * 
 * The guests who will visit the holiday village
 * 
 * Written by: 40208846
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem
{
    [Serializable]
    public class Guest
    {
        // Guest's age must be betweeen 1 and 101
        private int age;

        public int Age
        {
            get { return age; }
            set
            {
                if (value > 0 && value < 102)
                {
                    age = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Guest's age must be greater than 1 and less than 101");
                }

            }
        }

        // Guest's name cannot be blank
        private string name;

        public string Name
        {
            get { return name; }
            set
            {
                if (value == "")
                {
                    throw new ArgumentException("Name cannot be blank");
                }
                else
                {
                    name = value;
                }

            }
        }

        // Guest's passport number cannot be blank
        private string passportNumber;

        public string PassportNumber
        {
            get { return passportNumber; }
            set
            {
                if (value == "")
                {
                    throw new ArgumentException("Passport number cannot be blank");
                }
                else
                {
                    passportNumber = value;
                }
            }
        }

        // Guest constructor
        public Guest(string guestName, int guestAge, string passNo)
        {
            Name = guestName;
            Age = guestAge;
            PassportNumber = passNo;
        }
    }
}
