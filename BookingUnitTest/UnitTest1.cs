﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BookingUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void DepartureDate_After_ArrivalDate()
        {
            // Arrange
            var testCust = new ReservationSystem.Customer(1);
            
            // Act
            var bookingToBeTested = new ReservationSystem.Booking(1, testCust, new DateTime(2016, 01, 10), new DateTime(2016, 01, 01));

        }

        [TestMethod]
        public void GetCost_Works()
        {
            // Arrange
            var testCust = new ReservationSystem.Customer(1);
            var bookingToBeTested = new ReservationSystem.Booking(1, testCust, new DateTime(2016, 01, 01), new DateTime(2016, 01, 02));
            bookingToBeTested.GuestList.Add(new ReservationSystem.Guest("TestName", 20, "TestPassNumber"));
            double expectedCost = 50;

            // Act
            var actualInvoice = bookingToBeTested.GetCost();
            double actualCost;
            actualInvoice.TryGetValue("Booking", out actualCost);
            // Assert
            Assert.AreEqual(expectedCost, actualCost);


        }

        [TestMethod]
         public void GetGuestCount_Works()
        {
            // Arrange
            var testCust = new ReservationSystem.Customer(1);
            var bookingToBeTested = new ReservationSystem.Booking(1, testCust, new DateTime(2016, 01, 01), new DateTime(2016, 01, 02));
            bookingToBeTested.GuestList.Add(new ReservationSystem.Guest("Guest1", 20, "TestPassNumber1"));
            bookingToBeTested.GuestList.Add(new ReservationSystem.Guest("Guest2", 20, "TestPassNumber2"));
            int expectedGuestcount = 2;

            //Act
            int actualGuestCount = bookingToBeTested.GetGuestCount();

            //Assert
            Assert.AreEqual(expectedGuestcount, actualGuestCount);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Too_Many_Guests()
        {
            // Arrange
            var testCust = new ReservationSystem.Customer(1);
            var bookingToBeTested = new ReservationSystem.Booking(1, testCust, new DateTime(2016, 01, 01), new DateTime(2016, 01, 02));
            bookingToBeTested.AddGuest(new ReservationSystem.Guest("Guest1", 20, "TestPassNumber1"));
            bookingToBeTested.AddGuest(new ReservationSystem.Guest("Guest2", 20, "TestPassNumber2"));
            bookingToBeTested.AddGuest(new ReservationSystem.Guest("Guest3", 20, "TestPassNumber1"));
            bookingToBeTested.AddGuest(new ReservationSystem.Guest("Guest4", 20, "TestPassNumber2"));
                              
            // Act            
            bookingToBeTested.AddGuest(new ReservationSystem.Guest("Guest5", 20, "TestPassNumber1"));
        }
    }
}
